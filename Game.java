import java.util.Scanner;
import java.util.InputMismatchException;
public class Game{
	private int[][] board;
	private int turn;
	private Scanner in;
	public Game(){
		// 0 - empty
		// 1 - blue
		// 2 - red
		in = new Scanner(System.in);
		board = new int[6][7];
		turn = (int)(Math.random() * ((2 - 1) + 1)) + 1;
	}
	private void printBoard(){
		System.out.println();
		System.out.print("    ");
		for (int j=0;j<7;j++)
		System.out.print(j+1 + "  ");
		System.out.println();
		for (int i=0;i<6;i++){
			System.out.print(" " + (int)(i+1) + " ");
			
			for (int j=0;j<7;j++){
				if (board[i][j] == 0)
				System.out.print(" * ");
				else if (board[i][j] == 1)
				System.out.print(" B ");
				else System.out.print(" R ");
		}
		System.out.println();
	}
}
	private boolean winGame()
	{
		int b_count = 0;
		int r_count = 0;
		for (int i=0;i<6;i++)
		{
			for (int j=0;j<7;j++){
				//horizontally checks
				
				if (board[i][j] == 1) {++b_count;r_count =0;}
				else if (board[i][j] == 2) {++r_count;b_count = 0;}
				else if (board[i][j] == 0) b_count = r_count = 0;
				if (b_count == 4) {
					System.out.print("Player 1 (Blue) has won the game!");
					return true;
					}
				if (r_count == 4) {
					System.out.print("Player 2 (Red) has won the game!");
					return true;
					}
				// vertical checks
				if (i > 2) { 
					if (board[i-1][j] == 1 && board[i-2][j] == 1
					&& board[i-3][j] == 1 && board[i][j] == 1){
					System.out.print("Player 1 (Blue) has won the game!");
					return true;
					}
					if  (board[i-1][j] == 2 && board[i-2][j] == 2
					&& board[i-3][j] == 2 && board[i][j] == 2)
					{
					System.out.print("Player 2 (Red) has won the game!");
					return true;
					}
					
					}
				// diag check
				// N-E
				if (j <= 3 && i >= 3){
					if (board[i-1][j+1] == 1 && board[i-2][j+2] == 1 &&
					board[i-3][j+3] == 1 && board[i][j] == 1){
					System.out.print("Player 1 (Blue) has won the game!");
					return true;
					}
					if (board[i-1][j+1] == 2 && board[i-2][j+2] == 2 &&
					board[i-3][j+3] == 2 && board[i][j] == 2){
					System.out.print("Player 2 (Red) has won the game!");
					return true;
					}
					
				}
				// N-S
				if (j >= 3 && i>=3){
					if (board[i-1][j-1] == 1 && board[i-2][j-2] == 1 &&
					board[i-3][j-3] == 1 && board[i][j] == 1){
					System.out.print("Player 1 (Blue) has won the game!");
					return true;
					}
					if (board[i-1][j-1] == 2 && board[i-2][j-2] == 2 &&
					board[i-3][j-3] == 2 && board[i][j] == 2){
					System.out.print("Player 2 (Red) has won the game!");
					return true;
					}
				}
				// S-W
				if (j>=3 && i<=3){
					if (board[i+1][j-1] == 1 && board[i+2][j-2] == 1 &&
					board[i+3][j-3] == 1 && board[i][j] == 1){
					System.out.print("Player 1 (Blue) has won the game!");
					return true;
					}
					if (board[i+1][j-1] == 2 && board[i+2][j-2] == 2 &&
					board[i+3][j-3] == 2 && board[i][j] == 2){
					System.out.print("Player 2 (Red) has won the game!");
					return true;
					}
				}
				// E-S
				if (j <= 3 && i <=3 ){
					if (board[i+1][j+1] == 1 && board[i+2][j+2] == 1 &&
					board[i+3][j+3] == 1 && board[i][j] == 1){
					System.out.print("Player 1 (Blue) has won the game!");
					return true;
					}
					if (board[i+1][j+1] == 2 && board[i+2][j+2] == 2 &&
					board[i+3][j+3] == 2 && board[i][j] == 2){
					System.out.print("Player 2 (Red) has won the game!");
					return true;
					}
					
				}
			
			
			}
			
		}
		return false;
		
	}

	private void placeColor(int col,int color)
	{
		for (int i=0;i<6;i++)
			{
				 if (board[i][col] == 0 && i == 5) {
					 board[i][col] = color;
					 break;
					 }
				else if (board[i][col] !=0) {
					if (i == 0) {
						System.out.print("Column full, select another column.. ");
						if (color == 1) placeColor(handleInput(),1);
						else placeColor(handleInput(),2);
						return;
						}
					board[i-1][col]=color;break;}

			}
		
	}
	private boolean isTie()
	{
		for (int j=0;j<7;j++)
			if(board[0][j] == 0) 
			return false;
		return true;
	}
	private int handleInput()
	{
		int j_col;
				while (true){
				try{
					j_col = in.nextInt();
					if (j_col < 1 || j_col > 7) 
					System.out.print("Invalid column number.. select another column ");
					else break;
					}
				 catch (InputMismatchException e) {
				System.out.print("Invalid input. Please reenter: ");
				in.nextLine();
			}
		}
		return j_col - 1;
	
	}

	
	private int changeTurn(int turn)
	{
		if (turn == 1) return 2;
		return 1;
	}
	private void handleBluePlayer()
	{
			System.out.print("Player 1 (Blue) moves first ");
			int j_col = handleInput();
			placeColor(j_col,1);
			printBoard();
	}
	private void handleRedPlayer()
	{
			System.out.print("Player 2 (Red) moves first ");
			int j_col = handleInput();
			placeColor(j_col,2);
			printBoard();
	}
	
	public void play()
	{
		printBoard();
		System.out.println();
		do{
		if (turn == 1){
			handleBluePlayer();
		}
		else handleRedPlayer();
			System.out.println();
			turn = changeTurn(turn);
	}while(winGame() == false && isTie() == false);
	if (isTie()) System.out.println("Game ended with a tie\n");
	in.close();
	}
}

	

		
		


